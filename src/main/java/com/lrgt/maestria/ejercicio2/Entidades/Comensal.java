package com.lrgt.maestria.ejercicio2.Entidades;

import javax.persistence.*;
import java.util.List;

/**
 * Created by alumno on 12/07/2017.
 */
@Entity
public class Comensal {

    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String nombre, apellido;

    @OneToMany( targetEntity = Visita.class )
    private List visitas;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public List getVisitas() {
        return visitas;
    }

    public void setVisitas(List visitas) {
        this.visitas = visitas;
    }
}
