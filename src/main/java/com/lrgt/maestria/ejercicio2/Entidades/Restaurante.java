package com.lrgt.maestria.ejercicio2.Entidades;

import javax.persistence.*;
import java.util.List;

/**
 * Created by alumno on 12/07/2017.
 */
@Entity
public class Restaurante {

    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String nombre, direccion;
    private boolean aceptaNinios;
    private int categoria, cantidadTrabajadores, cantidadComensales;

    @OneToMany( targetEntity = Visita.class )
    private List visitas;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public boolean isAceptaNinios() {
        return aceptaNinios;
    }

    public void setAceptaNinios(boolean aceptaNinios) {
        this.aceptaNinios = aceptaNinios;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getCantidadTrabajadores() {
        return cantidadTrabajadores;
    }

    public void setCantidadTrabajadores(int cantidadTrabajadores) {
        this.cantidadTrabajadores = cantidadTrabajadores;
    }

    public int getCantidadComensales() {
        return cantidadComensales;
    }

    public void setCantidadComensales(int cantidadComensales) {
        this.cantidadComensales = cantidadComensales;
    }

    public float getImpuesto(){
        float impuesto = getCategoria() * getCantidadComensales();
        if(isAceptaNinios()) impuesto /= 3;
        return impuesto;
    }

    public List getVisitas() {
        return visitas;
    }

    public void setVisitas(List visitas) {
        this.visitas = visitas;
    }
}
