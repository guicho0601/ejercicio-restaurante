package com.lrgt.maestria.ejercicio2.Entidades;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by alumno on 12/07/2017.
 */
public interface RestauranteRepositorio extends JpaRepository<Restaurante, Long> {
}
