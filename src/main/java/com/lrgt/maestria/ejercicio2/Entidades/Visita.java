package com.lrgt.maestria.ejercicio2.Entidades;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by alumno on 12/07/2017.
 */
@Entity
public class Visita {

    @Id@GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private LocalDateTime fecha;

    @ManyToOne
    private Restaurante restaurante;
    @ManyToOne
    private Comensal comensal;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getFechaFormat(){
        return fecha.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    }

    public Restaurante getRestaurante() {
        return restaurante;
    }

    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }

    public Comensal getComensal() {
        return comensal;
    }

    public void setComensal(Comensal comensal) {
        this.comensal = comensal;
    }

    public String getNombreRestaurante(){
        return restaurante.getNombre();
    }
}
