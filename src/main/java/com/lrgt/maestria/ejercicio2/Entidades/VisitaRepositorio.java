package com.lrgt.maestria.ejercicio2.Entidades;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by alumno on 12/07/2017.
 */
public interface VisitaRepositorio extends JpaRepository<Visita, Long> {
    List<Visita> findByComensalId(Long id);
    List<Visita> findByRestauranteId(Long id);
}
