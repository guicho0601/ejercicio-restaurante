package com.lrgt.maestria.ejercicio2.Visual;

import com.lrgt.maestria.ejercicio2.Entidades.*;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

/**
 * Created by alumno on 12/07/2017.
 */
@SpringUI
public class Principal extends UI {

    @Autowired
    RestauranteRepositorio restauranteRepositorio;
    @Autowired
    ComensalRepositorio comensalRepositorio;
    @Autowired
    VisitaRepositorio visitaRepositorio;

    private Grid<Restaurante> restauranteGrid;
    private Grid<Comensal> comensalGrid;
    private Grid<Visita> visitaGrid;
    private VerticalLayout visitasComponents;
    private Comensal comensal_seleccionado;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        cargarDatos();
        setContent(getRestauranteComponents());
    }

    private void cargarDatos(){
        Restaurante restaurante = new Restaurante();
        restaurante.setNombre("Los Cofrades");
        restaurante.setDireccion("Cobán");
        restaurante.setCantidadTrabajadores(15);
        restaurante.setCantidadComensales(60);
        restaurante.setCategoria(4);
        restaurante.setAceptaNinios(true);
        restauranteRepositorio.save(restaurante);
        restaurante = new Restaurante();
        restaurante.setNombre("Las Luces");
        restaurante.setDireccion("Quetzaltenango");
        restaurante.setCantidadTrabajadores(20);
        restaurante.setCantidadComensales(80);
        restaurante.setCategoria(3);
        restaurante.setAceptaNinios(false);
        restauranteRepositorio.save(restaurante);
        Comensal comensal = new Comensal();
        comensal.setNombre("Luis");
        comensal.setApellido("Ramos");
        comensalRepositorio.save(comensal);
        comensal = new Comensal();
        comensal.setNombre("Pamela");
        comensal.setApellido("López");
        comensalRepositorio.save(comensal);
    }

    private VerticalLayout getRestauranteComponents() {
        VerticalLayout verticalLayout = new VerticalLayout();

        restauranteGrid = new Grid<>();
        restauranteGrid.addColumn(Restaurante::getId).setCaption("Id");
        restauranteGrid.addColumn(Restaurante::getNombre).setCaption("Nombre");
        restauranteGrid.addColumn(Restaurante::getDireccion).setCaption("Dirección");
        restauranteGrid.addColumn(Restaurante::getCategoria).setCaption("Categoria");
        restauranteGrid.addColumn(Restaurante::getCantidadTrabajadores).setCaption("Cantidad trabajadores");
        restauranteGrid.addColumn(Restaurante::getCantidadComensales).setCaption("Cantidad comensales");
        restauranteGrid.addColumn(Restaurante::getImpuesto).setCaption("Impuesto");
        restauranteGrid.setItems(restauranteRepositorio.findAll());
        restauranteGrid.setWidth("100%");

        Button button = new Button("Ir a Comensales");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                setContent(getComensalComponents());
            }
        });

        verticalLayout.addComponents(new Label("Restaurantes"), this.formularioRestaurante(), restauranteGrid, button);
        return verticalLayout;
    }

    private VerticalLayout formularioRestaurante() {
        TextField nombre = new TextField("Nombre");
        TextField direccion = new TextField("Dirección");
        TextField trabajadores = new TextField("Cantidad trabajadores");
        TextField comensales = new TextField("Cantidad comensales");
        CheckBox aceptaNinios = new CheckBox("Acepta niños", false);
        Slider categoria = new Slider("Categoria");
        categoria.setMin(1);
        categoria.setMax(5);
        categoria.setValue((double) 3);
        Button button = new Button("Agregar");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    Restaurante restaurante = new Restaurante();
                    restaurante.setNombre(nombre.getValue());
                    restaurante.setDireccion(direccion.getValue());
                    restaurante.setCantidadTrabajadores(Integer.parseInt(trabajadores.getValue()));
                    restaurante.setCantidadComensales(Integer.parseInt(comensales.getValue()));
                    restaurante.setCategoria(categoria.getValue().intValue());
                    restaurante.setAceptaNinios(aceptaNinios.getValue());
                    restauranteRepositorio.save(restaurante);
                    restauranteGrid.setItems(restauranteRepositorio.findAll());

                    nombre.clear();
                    direccion.clear();
                    trabajadores.clear();
                    comensales.clear();
                    categoria.setValue((double) 3);
                } catch (NumberFormatException e) {
                    Notification.show("Debe de ingresar solo números en los campos de trabajadores y comensales");
                }
            }
        });
        HorizontalLayout horizontalLayout = new HorizontalLayout(nombre, direccion, trabajadores, comensales);
        HorizontalLayout horizontalLayout1 = new HorizontalLayout(aceptaNinios, categoria, button);
        horizontalLayout1.setComponentAlignment(aceptaNinios, Alignment.MIDDLE_CENTER);
        return new VerticalLayout(horizontalLayout, horizontalLayout1);
    }

    private HorizontalLayout getComensalComponents() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        comensalGrid = new Grid<>();
        comensalGrid.addColumn(Comensal::getId).setCaption("Id");
        comensalGrid.addColumn(Comensal::getNombre).setCaption("Nombre");
        comensalGrid.addColumn(Comensal::getApellido).setCaption("Apellido");
        comensalGrid.setItems(comensalRepositorio.findAll());
        comensalGrid.setWidth("100%");

        comensalGrid.addItemClickListener(itemClick -> {
            comensal_seleccionado = itemClick.getItem();
            if(visitasComponents != null){
                horizontalLayout.removeComponent(visitasComponents);
            }
            visitasComponents = getVisitasComponents();
            horizontalLayout.addComponent(visitasComponents);
        });

        Button button = new Button("Ir a Restaurantes");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                setContent(getRestauranteComponents());
            }
        });
        horizontalLayout.addComponents(new VerticalLayout(new Label("Comensales"), this.getFormularioComensal(), comensalGrid, button));
        return horizontalLayout;
    }

    private HorizontalLayout getFormularioComensal() {
        TextField nombre = new TextField("Nombre");
        TextField apellido = new TextField("Apellido");
        Button button = new Button("Agregar");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Comensal comensal = new Comensal();
                comensal.setNombre(nombre.getValue());
                comensal.setApellido(apellido.getValue());
                comensalRepositorio.save(comensal);
                comensalGrid.setItems(comensalRepositorio.findAll());

                nombre.clear();
                apellido.clear();
            }
        });
        HorizontalLayout horizontalLayout = new HorizontalLayout(nombre, apellido, button);
        horizontalLayout.setComponentAlignment(button, Alignment.BOTTOM_CENTER);
        return horizontalLayout;
    }

    private VerticalLayout getVisitasComponents(){
        visitaGrid = new Grid<>();
        visitaGrid.addColumn(Visita::getNombreRestaurante).setCaption("Restaurante");
        visitaGrid.addColumn(Visita::getFechaFormat).setCaption("Fecha");
        visitaGrid.setItems(visitaRepositorio.findByComensalId(comensal_seleccionado.getId()));
        VerticalLayout verticalLayout = new VerticalLayout(getFomularioVisitas(), visitaGrid);
        verticalLayout.setMargin(true);
        return verticalLayout;
    }

    private HorizontalLayout getFomularioVisitas(){
        ComboBox<Restaurante> restauranteComboBox = new ComboBox<>("Restaurante", restauranteRepositorio.findAll());
        restauranteComboBox.setItemCaptionGenerator(Restaurante::getNombre);
        restauranteComboBox.setEmptySelectionAllowed(false);
        DateTimeField fecha = new DateTimeField("Fecha de visita");
        fecha.setValue(LocalDateTime.now());
        Button button = new Button("Agregar");
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                Visita visita = new Visita();
                visita.setComensal(comensal_seleccionado);
                visita.setRestaurante(restauranteComboBox.getValue());
                visita.setFecha(fecha.getValue());
                visitaRepositorio.save(visita);

                visitaGrid.setItems(visitaRepositorio.findByComensalId(comensal_seleccionado.getId()));
                fecha.setValue(LocalDateTime.now());
            }
        });
        HorizontalLayout horizontalLayout = new HorizontalLayout(restauranteComboBox, fecha, button);
        horizontalLayout.setComponentAlignment(button, Alignment.BOTTOM_CENTER);
        return horizontalLayout;
    }
}
